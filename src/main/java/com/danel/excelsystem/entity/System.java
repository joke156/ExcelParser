package com.danel.excelsystem.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "system")
public class System implements Serializable {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "support_group", length = 50)
    private String support_group;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupport_group() {
        return support_group;
    }

    public void setSupport_group(String support_group) {
        this.support_group = support_group;
    }

    @Override
    public String toString() {
        return "System{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", support_group='" + support_group + '\'' +
                '}';
    }
}
