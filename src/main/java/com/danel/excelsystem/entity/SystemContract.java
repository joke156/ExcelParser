package com.danel.excelsystem.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "system_contract")
public class SystemContract {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "active")
    private boolean active;

    @Column(name = "amount", precision = 19, scale = 2)
    private BigDecimal amount;

    @Column(name = "amount_period", length = 5)
    private String amountPeriod;

    @Column(name = "amount_type", length = 5)
    private String amountType;

    @Column(name = "authorization_percent", precision = 19, scale = 2)
    private BigDecimal authorizationPercent;

    @Column(name = "from_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;

    @Column(name = "order_number", length = 12)
    private String orderNumber;

    @Column(name = "request", length = 12)
    private String request;

    @Column(name = "to_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;

    @ManyToOne
    @JoinColumn(name = "system_id")
    private System system;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(String amountPeriod) {
        this.amountPeriod = amountPeriod;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public BigDecimal getAuthorizationPercent() {
        return authorizationPercent;
    }

    public void setAuthorizationPercent(BigDecimal authorizationPercent) {
        this.authorizationPercent = authorizationPercent;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "SystemContract{" +
                "id=" + id +
                ", active=" + active +
                ", amount=" + amount +
                ", amountPeriod='" + amountPeriod + '\'' +
                ", amountType='" + amountType + '\'' +
                ", authorizationPercent=" + authorizationPercent +
                ", fromDate=" + fromDate +
                ", orderNumber='" + orderNumber + '\'' +
                ", request='" + request + '\'' +
                ", toDate=" + toDate +
                ", system=" + system +
                '}';
    }
}
