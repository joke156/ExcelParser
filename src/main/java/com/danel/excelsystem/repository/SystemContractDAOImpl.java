package com.danel.excelsystem.repository;

import com.danel.excelsystem.entity.SystemContract;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class SystemContractDAOImpl implements SystemContractDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SystemDAO system;

    public SystemContract findById(Long id) {
        return sessionFactory.getCurrentSession().find(SystemContract.class, id);
    }

    public void add(SystemContract systemContract) {
        sessionFactory.getCurrentSession().save(systemContract);
    }

    public void update(SystemContract systemContract) {
        sessionFactory.getCurrentSession().update(systemContract);
    }

    public void delete(SystemContract systemContract) {
        sessionFactory.getCurrentSession().delete(systemContract);
    }

    public List<SystemContract> findAll() {
        return sessionFactory.getCurrentSession().createQuery("From SystemContract s").list();
    }

    public SystemContract findByRequestAndSystem(String request, Long system_id) {
        try {

            return (SystemContract) sessionFactory.getCurrentSession().createQuery("Select sc from SystemContract  sc WHERE sc.request=:request and sc.system=:system").setParameter("request", request).setParameter("system", system.findById(Long.valueOf(system_id))).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


}
