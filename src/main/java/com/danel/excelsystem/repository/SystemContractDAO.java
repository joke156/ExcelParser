package com.danel.excelsystem.repository;

import com.danel.excelsystem.entity.SystemContract;

import java.util.List;

public interface SystemContractDAO {

    SystemContract findById(Long id);

    SystemContract findByRequestAndSystem(String request, Long system_id);

    void add(SystemContract systemContract);

    void update(SystemContract systemContract);

    void delete(SystemContract systemContract);

    List<SystemContract> findAll();

}
