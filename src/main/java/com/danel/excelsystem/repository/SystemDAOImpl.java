package com.danel.excelsystem.repository;

import com.danel.excelsystem.entity.System;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SystemDAOImpl implements SystemDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public System findById(Long id) {
        return sessionFactory.getCurrentSession().find(System.class, id);
    }

    public System findByName(String name) {
        Query byName = sessionFactory.getCurrentSession().createQuery("From System s WHERE s.name=:name");
        byName.setParameter("name", name);
        return (System) byName.getSingleResult();
    }

    public List<System> findAll() {
        return sessionFactory.getCurrentSession().createQuery("From System s").list();
    }

    public void add(System system) {
        sessionFactory.getCurrentSession().save(system);
    }

    public void update(System system) {
        sessionFactory.getCurrentSession().update(system);
    }

    public void delete(System system) {
        sessionFactory.getCurrentSession().delete(system);
    }

}
