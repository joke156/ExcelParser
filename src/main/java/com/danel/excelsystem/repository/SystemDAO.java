package com.danel.excelsystem.repository;

import com.danel.excelsystem.entity.System;

import java.util.List;

public interface SystemDAO {

    System findById(Long id);

    System findByName(String name);

    List<System> findAll();

    void add(System system);

    void update(System system);

    void delete(System system);
}
