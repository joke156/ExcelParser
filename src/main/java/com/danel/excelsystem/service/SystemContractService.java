package com.danel.excelsystem.service;

import com.danel.excelsystem.dto.SystemContractDTO;
import com.danel.excelsystem.entity.SystemContract;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SystemContractService {

    SystemContract findById(Long id);

    void add(MultipartFile file);

    void addForm(SystemContractDTO systemContractDTO);

    void update(SystemContractDTO systemContract);

    void delete(SystemContract systemContract);

    SystemContract findByRequestAndSystem(String request, Long system_id);

    List<SystemContract> findAll();
}
