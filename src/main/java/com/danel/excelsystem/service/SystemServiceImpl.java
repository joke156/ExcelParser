package com.danel.excelsystem.service;

import com.danel.excelsystem.entity.System;
import com.danel.excelsystem.repository.SystemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SystemServiceImpl implements SystemService {

    @Autowired
    private SystemDAO systemDAO;

    public System findById(Long id) {
        return systemDAO.findById(Long.valueOf(id));
    }

    public System findByName(String name) {
        return systemDAO.findByName(name);
    }

    public void add(System system) {
        systemDAO.add(system);
    }

    public void update(System system) {
        systemDAO.update(system);
    }

    public void delete(System system) {
        systemDAO.delete(system);
    }

    public List<System> findAll() {
        return systemDAO.findAll();
    }

}
