package com.danel.excelsystem.service;

import com.danel.excelsystem.dto.SystemContractDTO;
import com.danel.excelsystem.entity.SystemContract;
import com.danel.excelsystem.repository.SystemContractDAO;
import com.danel.excelsystem.repository.SystemDAO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class SystemContractServiceImpl implements SystemContractService {

    @Autowired
    private SystemContractDAO systemContractDAO;

    @Autowired
    private SystemDAO systemDAO;

    public SystemContract findById(Long id) {
        return systemContractDAO.findById(id);
    }

    public void add(MultipartFile file) {

        String filename = file.getOriginalFilename();
        int index = filename.lastIndexOf('.');
        String substring = filename.substring(index, filename.length());
        Workbook workBook = null;
        switch (substring) {
            case ".xls": {
                try {
                    workBook = new HSSFWorkbook(new POIFSFileSystem(file.getInputStream()));
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            case ".xlsx": {
                try {
                    workBook = new XSSFWorkbook(file.getInputStream());
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (workBook != null && workBook.getNumberOfSheets() > 0) {

            for (Row row : workBook.getSheetAt(0)) {

                if (row.getRowNum() == 0) {
                    continue;
                }

                SystemContract systemContract = new SystemContract();

                systemContract.setSystem(systemDAO.findByName(row.getCell(0).toString()));
                systemContract.setRequest(row.getCell(1).toString());
                systemContract.setOrderNumber(row.getCell(2).toString());
                systemContract.setFromDate(row.getCell(3).getDateCellValue());
                systemContract.setToDate(row.getCell(4).getDateCellValue());
                BigDecimal amount = new BigDecimal(row.getCell(5).toString());
                systemContract.setAmount(amount);
                systemContract.setAmountType(row.getCell(6).toString());
                systemContract.setAmountPeriod(row.getCell(7).toString());
                BigDecimal percent = new BigDecimal(row.getCell(8).toString());
                systemContract.setAuthorizationPercent(percent);
                systemContract.setActive(Boolean.parseBoolean(row.getCell(9).getStringCellValue()));
                try {
                    systemContractDAO.add(systemContract);
                } catch (ConstraintViolationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addForm(SystemContractDTO systemContractDTO) {

        SystemContract systemContract = new SystemContract();
        systemContract.setActive(systemContractDTO.isActive());
        BigDecimal amount = new BigDecimal(systemContractDTO.getAmount());
        systemContract.setAmount(amount);
        systemContract.setAmountPeriod(systemContractDTO.getAmountPeriod());
        systemContract.setAmountType(systemContractDTO.getAmountType());
        BigDecimal percent = new BigDecimal(systemContractDTO.getAuthorizationPercent());
        systemContract.setAuthorizationPercent(percent);
        systemContract.setFromDate(systemContractDTO.getFromDate());
        systemContract.setToDate(systemContractDTO.getToDate());
        systemContract.setOrderNumber(systemContractDTO.getOrderNumber());
        systemContract.setRequest(systemContractDTO.getRequest());
        systemContract.setSystem(systemContractDTO.getSystem());

        try {
            systemContractDAO.add(systemContract);
        } catch (ConstraintViolationException | DataIntegrityViolationException e) {

        }
    }

    public SystemContract findByRequestAndSystem(String request, Long system_id) {
        return systemContractDAO.findByRequestAndSystem(request, system_id);
    }

    public void delete(SystemContract systemContract) {
        systemContractDAO.delete(systemContract);
    }

    public void update(SystemContractDTO systemContract) {

        SystemContract contract = systemContractDAO.findById(systemContract.getId());

        contract.setActive(systemContract.isActive());
        BigDecimal amount = new BigDecimal(systemContract.getAmount());
        contract.setAmount(amount);
        contract.setAmountPeriod(systemContract.getAmountPeriod());
        contract.setAmountType(systemContract.getAmountType());
        BigDecimal percent = new BigDecimal(systemContract.getAuthorizationPercent());
        contract.setAuthorizationPercent(percent);
        contract.setFromDate(systemContract.getFromDate());
        contract.setToDate(systemContract.getToDate());
        contract.setOrderNumber(systemContract.getOrderNumber());
        contract.setRequest(systemContract.getRequest());
        contract.setSystem(systemContract.getSystem());

        systemContractDAO.update(contract);

    }

    public List<SystemContract> findAll() {
        return systemContractDAO.findAll();
    }

}
