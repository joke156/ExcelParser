package com.danel.excelsystem.validator;

import com.danel.excelsystem.dto.FileDTO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.io.IOException;
import java.util.regex.Pattern;

@Component
public class FileValidator implements Validator {

    @Autowired
    private Environment environment;

    public boolean supports(Class<?> clazz) {
        return FileDTO.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        FileDTO file = (FileDTO) target;

        if (file.getFile() == null || file.getFile().isEmpty()) {
            errors.rejectValue("file", "empty", environment.getRequiredProperty("empty"));
        } else {
            String name = file.getFile().getOriginalFilename();
            int index = name.lastIndexOf('.');
            String substring = name.substring(index, name.length());
            Workbook workBook = null;
            switch (substring) {
                case ".xls": {
                    try {
                        workBook = new HSSFWorkbook(new POIFSFileSystem(file.getFile().getInputStream()));
                    } catch (IOException e) {
                        errors.rejectValue("file", "empty", environment.getRequiredProperty("empty"));
                    } catch (OfficeXmlFileException e) {
                        errors.rejectValue("file", "strange", environment.getRequiredProperty("strange"));
                    }
                    break;
                }
                case ".xlsx": {
                    try {
                        workBook = new XSSFWorkbook(file.getFile().getInputStream());
                    } catch (IOException e) {
                        errors.rejectValue("file", "empty", environment.getRequiredProperty("empty"));
                    } catch (OfficeXmlFileException e) {
                        errors.rejectValue("file", "strange", environment.getRequiredProperty("strange"));
                    }
                    break;
                }
                default: {
                    errors.rejectValue("file", "type", environment.getRequiredProperty("type"));
                }
            }
            if (workBook == null) {
                errors.rejectValue("file", "strange", environment.getRequiredProperty("strange"));
                return;
            }

            if (workBook.getNumberOfSheets() > 0) {
                for (Row row : workBook.getSheetAt(0)) {

                    if (row.getRowNum() == 0) {
                        continue;
                    }

                    try {
                        if (!Pattern.matches(environment.getRequiredProperty("pattern1"), row.getCell(0).getStringCellValue())) {
                            errors.rejectValue("file", "cell1", environment.getRequiredProperty("cell1"));
                        }
                        try {
                            Double cellValue = row.getCell(1).getNumericCellValue();
                            if (!Pattern.matches(environment.getRequiredProperty("pattern2"), cellValue.toString())) {
                                errors.rejectValue("file", "cell2", environment.getRequiredProperty("cell2"));
                            }
                        } catch (IllegalStateException | NumberFormatException e) {
                            errors.rejectValue("file", "cell2", environment.getRequiredProperty("cell2"));
                        }
                        if (!Pattern.matches(environment.getRequiredProperty("pattern3"), row.getCell(2).getStringCellValue())) {
                            errors.rejectValue("file", "cell3", environment.getRequiredProperty("cell3"));
                        }
                        try {
                            row.getCell(3).getDateCellValue();
                        } catch (IllegalStateException | NumberFormatException e) {
                            errors.rejectValue("file", "cell4", environment.getRequiredProperty("cell4"));
                        }
                        try {
                            row.getCell(4).getDateCellValue();
                        } catch (IllegalStateException | NumberFormatException e) {
                            errors.rejectValue("file", "cell5", environment.getRequiredProperty("cell5"));
                        }
                        if (!Pattern.matches(environment.getRequiredProperty("pattern6"), row.getCell(5).getStringCellValue())) {
                            errors.rejectValue("file", "cell6", environment.getRequiredProperty("cell6"));
                        }
                        if (!Pattern.matches(environment.getRequiredProperty("pattern7"), row.getCell(6).getStringCellValue())) {
                            errors.rejectValue("file", "cell7", environment.getRequiredProperty("cell7"));
                        }
                        if (!Pattern.matches(environment.getRequiredProperty("pattern8"), row.getCell(7).getStringCellValue())) {
                            errors.rejectValue("file", "cell8", environment.getRequiredProperty("cell8"));
                        }
                        try {
                            Double percent = row.getCell(8).getNumericCellValue();
                            if (!Pattern.matches(environment.getRequiredProperty("pattern9"), percent.toString())) {
                                errors.rejectValue("file", "cell9", environment.getRequiredProperty("cell9"));
                            }
                        } catch (IllegalStateException | NumberFormatException e) {
                            errors.rejectValue("file", "cell9", environment.getRequiredProperty("cell9"));
                        }
                        try {
                            if (!Pattern.matches(environment.getRequiredProperty("pattern10"), row.getCell(9).getStringCellValue())) {
                                errors.rejectValue("file", "cell10", environment.getRequiredProperty("cell10"));
                            }
                        } catch (IllegalStateException e) {
                            errors.rejectValue("file", "cell10", environment.getRequiredProperty("cell10"));
                        }
                    } catch (NullPointerException e) {
                        errors.rejectValue("file", environment.getRequiredProperty("empty"));
                    }
                }

            } else {
                errors.rejectValue("file", "nothing", environment.getRequiredProperty("nothing"));
            }
        }
    }
}
