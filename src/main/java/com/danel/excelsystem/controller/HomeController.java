package com.danel.excelsystem.controller;

import com.danel.excelsystem.dto.FileDTO;
import com.danel.excelsystem.dto.SystemContractDTO;
import com.danel.excelsystem.entity.SystemContract;
import com.danel.excelsystem.repository.SystemContractDAO;
import com.danel.excelsystem.service.SystemContractService;
import com.danel.excelsystem.service.SystemService;
import com.danel.excelsystem.service.SystemServiceImpl;
import com.danel.excelsystem.validator.FileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private SystemContractService systemContractService;

    @Autowired
    private SystemService systemService;

    @Autowired
    private FileValidator fileValidator;

    @Autowired
    Environment environment;

    @InitBinder("excel")
    protected void initBinderFileValidator(WebDataBinder binder) {
        binder.setValidator(fileValidator);
    }

    @InitBinder("contract")
    protected void initBinder(WebDataBinder binder) {

    }

    @GetMapping(value = "/home")

    private String home(Model model) {

        model.addAttribute("excel", new FileDTO());
        model.addAttribute("contract", new SystemContractDTO());
        model.addAttribute("systems", systemService.findAll());
        model.addAttribute("contracts", systemContractService.findAll());

        return "index";
    }

    @PostMapping(value = "/add")
    private String add(Model model, @Valid @ModelAttribute("excel") FileDTO fileDTO, BindingResult result, Errors errors) {

        model.addAttribute("excel", new FileDTO());
        model.addAttribute("contract", new SystemContract());
        model.addAttribute("systems", systemService.findAll());
        model.addAttribute("contracts", systemContractService.findAll());

        if (result.hasErrors()) {
            List<FieldError> error = errors.getFieldErrors();
            model.addAttribute("errors", error);
            return "index";
        }
        try {
            systemContractService.add(fileDTO.getFile());
        } catch (ConstraintViolationException | DataIntegrityViolationException e) {
            model.addAttribute("duplicate", environment.getRequiredProperty("duplicate"));
        } catch (NullPointerException e) {
            model.addAttribute("duplicate", "Nieznany bład w pliku importu");
        }
        return "redirect:/home";
    }

    @PostMapping(value = "/addform")
    private ModelAndView addForm(@Valid @ModelAttribute("contract") SystemContractDTO SystemContractDTO, Errors errors, BindingResult result) {

        ModelAndView modelAndView = new ModelAndView("index");

        modelAndView.addObject("excel", new FileDTO());
        modelAndView.addObject("contract", new SystemContract());
        modelAndView.addObject("systems", systemService.findAll());
        modelAndView.addObject("contracts", systemContractService.findAll());

        if (result.hasErrors()) {
            modelAndView.addObject("duplicate", "Nie udalo sie dodac");
            System.out.println(SystemContractDTO.toString());
            return modelAndView;
        }
        try {
            if (SystemContractDTO.getId() != null || systemContractService.findById(SystemContractDTO.getId()) != null) {
                systemContractService.update(SystemContractDTO);
            } else {
                systemContractService.addForm(SystemContractDTO);
            }
        } catch (ConstraintViolationException | DataIntegrityViolationException e) {
            modelAndView.addObject("duplicate", environment.getRequiredProperty("duplicate"));
        }
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping("delete")
    public ModelAndView delete(@RequestParam(value = "request", required = true) String request, @RequestParam(value = "system", required = true) String system) {
        ModelAndView modelAndView = new ModelAndView("redirect:/home");
        Long system_id = systemService.findByName(system).getId();
        if (null != systemContractService.findByRequestAndSystem(request, system_id)) {
            systemContractService.delete(systemContractService.findByRequestAndSystem(request, system_id));
        }
        return modelAndView;
    }
}
