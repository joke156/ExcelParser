package com.danel.excelsystem.dto;

import com.danel.excelsystem.entity.System;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class SystemContractDTO {

    private Long id;

    private boolean active;

    @NotNull
    @Pattern(regexp = "[0-9]{1,19}|[0-9]{0,17}\\.[0-9]{0,2}", message = "Zly format danych")
    private String amount;

    @NotBlank
    @Length(min = 1, max = 5)
    private String amountPeriod;


    @NotBlank
    @Length(max = 5)
    private String amountType;

    @NotNull
    @Length(min = 1, max = 19)
    @Pattern(regexp = "[0-9]{1,19}|[0-9]{0,17}\\.[0-9]{0,2}", message = "Zly format danych")
    private String authorizationPercent;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fromDate;

    @NotBlank
    @Length(max = 12)
    private String orderNumber;

    @NotBlank
    @Length(max = 12)
    private String request;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date toDate;

    @NotNull
    private System system;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(String amount_period) {
        this.amountPeriod = amount_period;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getAuthorizationPercent() {
        return authorizationPercent;
    }

    public void setAuthorizationPercent(String authorizationPercent) {
        this.authorizationPercent = authorizationPercent;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "SystemContractDTO{" +
                "id=" + id +
                ", active=" + active +
                ", amount='" + amount + '\'' +
                ", amountPeriod='" + amountPeriod + '\'' +
                ", amountType='" + amountType + '\'' +
                ", authorizationPercent='" + authorizationPercent + '\'' +
                ", fromDate=" + fromDate +
                ", orderNumber='" + orderNumber + '\'' +
                ", request='" + request + '\'' +
                ", toDate=" + toDate +
                ", system=" + system +
                '}';
    }
}
