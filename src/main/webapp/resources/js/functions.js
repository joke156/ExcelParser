function validate() {

    $("#error").empty().hide();

    var active = document.forms["addRowWithForm"]["active"].value;
    var amount = document.forms["addRowWithForm"]["amount"].value;
    var amountPeriod = document.forms["addRowWithForm"]["amountPeriod"].value;
    var amountType = document.forms["addRowWithForm"]["amountType"].value;
    var authorizationPercent = document.forms["addRowWithForm"]["authorizationPercent"].value;
    var fromDate = document.forms["addRowWithForm"]["fromDate"].value;
    var toDate = document.forms["addRowWithForm"]["toDate"].value;
    var orderNumber = document.forms["addRowWithForm"]["orderNumber"].value;
    var request = document.forms["addRowWithForm"]["request"].value;
    var system = document.forms["addRowWithForm"]["system"].value;
    new RegExp("[0-9]{1,19}|[0-9]{0,17}\.[0-9]{0,2}");

    if (active === "" || amount === "" || amountPeriod === "" || amountType === "" || authorizationPercent === "" || fromDate === "" || toDate === "" || orderNumber === "" || request === "" || system === "") {
        $(document).ready(function () {
            $("#error").append("Musisz wypelnic wszystkie pola <br/> ").show();
        });
        return false;
    }
    if (active !== "false" && active !== "true") {
        $(document).ready(function () {
            $("#error").append("Musisz podac wartosc true/false w pierwszej kolumnie <br/> ").show();
        });
        return false;
    }
}

$(document).ready(function () {

    $("tr").click(function () {
        var id = $(this).find('.id').html();
        var active = $(this).find('.active').html();
        var amount = $(this).find('.amount').html();
        var amountPeriod = $(this).find('.amountPeriod').html();
        var amountType = $(this).find('.amountType').html();
        var authorizationPercent = $(this).find('.authorizationPercent').html();
        var fromDate = $(this).find('.fromDate').html();
        var toDate = $(this).find('.toDate').html();
        var orderNumber = $(this).find('.orderNumber').html();
        var request = $(this).find('.request').html();
        var system = $(this).find('.system').html();

        $("#id").val(id);
        $("#active").val(active);
        $("#amount").val(amount);
        $("#amountPeriod").val(amountPeriod);
        $("#amountType").val(amountType);
        $("#authorizationPercent").val(authorizationPercent);
        $("#fromDate").val(fromDate);
        $("#toDate").val(toDate);
        $("#orderNumber").val(orderNumber);
        $("#request").val(request);
        $("#system").val(system);

    });
    $("#contracts tbody tr td .delete").click(function () {
        var request = $(this).parent().parent().find('.request').html();
        var system = $(this).parent().parent().find('.system').html();
        var del = confirm("Czy napewno chcesz usunac?")
        if (del === true) {
            window.location.replace("http://localhost:8080/delete?request=" +
                request + "&system=" + system)
        }
    });
});

