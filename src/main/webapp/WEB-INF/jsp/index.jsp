<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form:form name="addRow" action="add" method="post" modelAttribute="excel" enctype="multipart/form-data">
    <form:input path="file" type="file" required="required"/>
    <input type="submit" value="Dodaj"/> <br/>
    <c:if test="${not empty errors}">
        <div class="alert alert-danger">
            Nie udało się dodać pliku, wystąpiły następujące błędy:<br/>
            <c:forEach var="error" items="${errors}">
                ${error.getDefaultMessage()}<br/>
            </c:forEach>
        </div>
    </c:if>
    <c:if test="${not empty success}">
        <div class="alert alert-danger">
                ${success}<br/>
        </div>
    </c:if>
    <c:if test="${not empty duplicate}">
        <div class="alert alert-danger">
                ${duplicate}<br/>
        </div>
    </c:if>
</form:form>
<table id="contracts">
    <thead>
    <th>Id</th>
    <th>Active</th>
    <th>Amount</th>
    <th>Amount Period</th>
    <th>Amount Type</th>
    <th>Authorization Percent</th>
    <th>From Date</th>
    <th>To Date</th>
    <th>Order Number</th>
    <th>Request</th>
    <th>System</th>
    <th>Usuń rekord</th>
    </thead>
    <tbody>

    <c:forEach items="${contracts}" var="contract">
        <tr>
            <td class="id">${contract.id}</td>
            <td class="active">${contract.active}</td>
            <td class="amount">${contract.amount}</td>
            <td class="amountPeriod">${contract.amountPeriod}</td>
            <td class="amountType">${contract.amountType}</td>
            <td class="authorizationPercent">${contract.authorizationPercent}</td>
            <td class="fromDate">${contract.fromDate}</td>
            <td class="toDate">${contract.toDate}</td>
            <td class="orderNumber">${contract.orderNumber}</td>
            <td class="request">${contract.request}</td>
            <td class="system">${contract.system.name}</td>
            <td>
                <button class="delete">Usuń</button>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<br/>
<p id="confirm"></p><br/>
<h1>Formularz dodawania/edytowania rekordów: </h1>
<form:form name="addRowWithForm" action="addform" modelAttribute="contract" method="post" onsubmit="return validate()">
    <form:input path="id" id="id" type="hidden"/>
    <label>Active: </label>
    <form:input path="active" id="active" placeHolder="true/false" type="text"/><br/>
    <form:errors path="active"/><br/>
    <label>Amount: </label>
    <form:input path="amount" id="amount" placeHolder="Amount" type="text"/><br/>
    <form:errors path="amount"/><br/>
    <label>Amount Period: </label>
    <form:input path="amountPeriod" id="amountPeriod" placeHolder="Amount Period" type="text"/><br/>
    <form:errors path="amountPeriod"/><br/>
    <label>Amount Type: </label>
    <form:input path="amountType" id="amountType" placeHolder="Amount type" type="text"/><br/>
    <form:errors path="amountType"/><br/>
    <label>Authorization Percent: </label>
    <form:input path="authorizationPercent" id="authorizationPercent" placeHolder="Authorization Percent"
                type="text"/><br/>
    <form:errors path="authorizationPercent"/><br/>
    <label>From Date: </label>
    <form:input path="fromDate" id="fromDate" placeHolder="From_date" type="date"/><br/>
    <form:errors path="fromDate"/><br/>
    <label>To Date: </label>
    <form:input path="toDate" id="toDate" placeHolder="To Date" type="date"/><br/>
    <form:errors path="toDate"/><br/>
    <label>Order Number: </label>
    <form:input path="orderNumber" id="orderNumber" placeHolder="Order Number" type="text"/><br/>
    <form:errors path="orderNumber"/><br/>
    <label>Request: </label>
    <form:input path="request" id="request" placeHolder="Request" type="text"/><br/>
    <form:errors path="request"/><br/>
    <label>System: </label>
    <form:select path="system.id" id="system" size="3" items="${systems}" itemLabel="name" itemValue="id"/><br/>
    <form:errors path="system.id"/><br/>
    <input type="submit" value="Dodaj/Zmien"/><br/>
    <p id="error" class="alert alert-warning" style="display: none"></p>
</form:form>